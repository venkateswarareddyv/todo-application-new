function strikeThrough(selectedTask) {
    let taskName = selectedTask.parentElement;
    if (selectedTask.checked) {
      taskName.classList.add("text");
      todos[selectedTask.id].status = "completed";
    } else {
      taskName.classList.remove("text");
      todos[selectedTask.id].status = "pending";
      showToDo("All");
    }
    todos = [...todos];
  }
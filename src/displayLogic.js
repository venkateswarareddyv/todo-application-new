function showToDo(value) {
  taskInput.focus()
  if (value === "All") {
    clearAllButton.style.display = "none";
    inputContainer.style.display = "block";
    let div = "";
    if (todos) {
      todos.forEach((todo, key) => {
        let isCompleted = todo.status === "completed" ? "checked" : "";
        let isCompleted1 = todo.status === "completed" ? "text" : "";
        div += `<div class="createdContainer" id=${key}>
                <label for=${key} class="labelElement ${isCompleted1}">
                    <input type="checkbox" id=${key} class="checkbox" onclick="strikeThrough(this)" ${isCompleted}>
                    ${todo.name}
                </label>
                <i class="material-icons buttonImg hidden" onclick="deleteSelected(this)" id=${key}>delete</i>
            </div>`;
      });
      let deletebtnlist = document.querySelectorAll(".material-icons");
      deletebtnlist.forEach((btn) => {
        btn.classList.add("hidden");
      });
    }
    taskBox.innerHTML = div;
  } else if (value === "pending") {
    clearAllButton.style.display = "none";
    taskBox.innerHTML = "";
    inputContainer.style.display = "block";
    let div = "";
    if (todos) {
      todos.forEach((todo, key) => {
        let isCompleted = todo.status === "completed" ? "checked" : "";
        let isCompleted1 = todo.status === "completed" ? "text" : "";
        if (todo.status === "pending") {
          div += `<div class="createdContainer" id=${key}>
                    <label for=${key} class="labelElement ${isCompleted1}">
                        <input type="checkbox" id=${key} class="checkbox" onclick="strikeThrough(this)" ${isCompleted}>
                        ${todo.name}
                    </label>
                    <i class="material-icons buttonImg hidden" onclick="deleteSelected(this)" id=${key}>delete</i>
                </div>`;
        }
      });
      let deletebtnlist = document.querySelectorAll(".material-icons");
      deletebtnlist.forEach((btn) => {
        btn.classList.add("hidden");
      });
    }
    taskBox.innerHTML = div;
  } else if (value === "completed") {
    taskBox.innerHTML = "";
    inputContainer.style.display = "none";
    let div = "";
    if (todos) {
      todos.forEach((todo, key) => {
        let isCompleted = todo.status === "completed" ? "checked" : "";
        let isCompleted1 = todo.status === "completed" ? "text" : "";
        if (todo.status === "completed") {
          clearAllButton.style.display = "block";
          div += `<div class="createdContainer" id=${key}>
                    <label for=${key} class="labelElement ${isCompleted1}">
                        <input type="checkbox" id=${key} class="checkbox" onclick="strikeThrough(this)" ${isCompleted}>
                        ${todo.name}
                    </label>
                    <i class="fa fa-trash buttonImg" onclick="deleteSelected(this)" id=${key} aria-hidden="true"></i>
                </div>`;
        }
      });
    }
    taskBox.innerHTML = div;
  }
}

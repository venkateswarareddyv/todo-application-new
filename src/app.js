let taskInput = document.getElementById("inputcontainer");
taskInput.focus();
let taskBox = document.getElementById("to-do-container");

let clearAllButton = document.querySelector(".button1");
let inputContainer = document.getElementById('inputButton');

let todos = [];
let count = 0;

clearAllButton.addEventListener("click", () => {
  const filteredList = todos.filter((element) => element.status === "pending");
  todos = [...filteredList];
  showToDo("completed");
});

function deleteSelected(selectedDeletedImage) {
  let data = selectedDeletedImage.parentElement
  todos.splice(data.id, 1);
  showToDo("completed")
}

inputContainer.addEventListener('submit', function (event) {
  event.preventDefault()
  let userInput = taskInput.value.trim();
  if (userInput) {
    taskInput.value = "";
    let taskInfo = { name: userInput, status: "pending" };
    todos.push(taskInfo);
    showToDo("All");
  }
})